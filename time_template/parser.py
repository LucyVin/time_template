"""
This library is intended to perform as a lightweight, no-dependancies parser
of strictly structured datetime configuration.

Notation is similar to writing standard python datetime operations.

date() || d() => datetime.date() object
datetime() || dt() => datetime.datetime() object
timedelta() || td() => datetime.timedelta() object

additionally, Date contains functions which operate on a date or datetime object
to return specific interval dates, such as the start and end of a given object's
quarter, month, or year.
"""
import operator
from datetime import date, datetime, timedelta
from time_template.date import Date

OP_ORDER = ["*", "/", "//", "%", "+", "-"]

class TimeParseError(Exception):
    pass

class TemplateParser:
    def __init__(self, template_string=None):
        if template_string:
            #self.as_time = self.parse_time(template_string)
            pass

        self.template_string = template_string

    @property
    def _obj_map(self) -> dict:
        return {
            "dt":datetime,
            "datetime":datetime,
            "d":date,
            "date":date,
            "td":timedelta,
            "timedelta":timedelta
        }

    @property
    def _op_map(self) -> dict: 
        return {
            "*":operator.mul,
            "/":operator.truediv,
            "//":operator.floordiv,
            "%":operator.mod,
            "+":operator.add,
            "-":operator.sub,
        }

    

    