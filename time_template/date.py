import datetime
from datetime import timedelta

class Date:
    def __init__(self, dt=None):
        if not dt:
            self.dt = datetime.datetime.today()

        else:
            self.dt = dt

    @property
    def today(self):
        return self.dt

    @property
    def tomorrow(self):
        return self.dt + timedelta(days=1)

    @property
    def yesterday(self):
        return self.dt - timedelta(days=1)

    @property
    def month_start(self):
        dt = self._zero_if_datetime(self.dt)
        return dt.replace(day=1)

    @property
    def month_end(self):
        dt = self._max_if_datetime(self.dt)

        d = dt.replace(
            day=1, 
            month=self.dt.month+1
            ) 
        d = d - timedelta(days=1)

        return d

    @property    
    def quarter_start(self):
        dt = self._zero_if_datetime(self.dt)

        return dt.replace(
            year=dt.year,
            month=(3*self.quarter)-2,
            day=1
            )

    @property
    def quarter_end(self):
        dt = self._max_if_datetime(self.dt)

        month = 3 * self.quarter
        r = int(month/12)

        d = dt.replace(
            year=dt.year+r,
            month=month%12+1,
            day=1
        ) 
        
        d = d - timedelta(days=1)

        return d

    @property
    def year_start(self):
        dt = self._zero_if_datetime(self.dt)
        
        return dt.replace(day=1, month=1)

    @property
    def year_end(self):
        dt = self._max_if_datetime(self.dt)

        return dt.replace(day=31, month=12)

    @property
    def quarter(self):
        return ((self.dt.month - 1) // 3) + 1

    def _zero_time(self, dt):
        return dt.replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0
        )
    
    def _max_time(self, dt):
        d = self._zero_time(dt) + timedelta(days=1)
        return d - timedelta(microseconds=1)


    def _zero_if_datetime(self, dt):
        if isinstance(dt, datetime.datetime):
            dt = self._zero_time(dt)

        return dt

    def _max_if_datetime(self, dt):
        if isinstance(dt, datetime.datetime):
            dt = self._max_time(dt)

        return dt
