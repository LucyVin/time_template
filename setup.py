from setuptools import setup, find_packages

setup(
    name="time_template",
    version="0.1",
    packages=find_packages(),
    author="LucyVin",
    author_email="lucy@lucyvin.com",
    description="small, lightweight library for dynamic datetime operations stored in plain text",
    license="MIT",
    keywords="datetime operations plaintext string dynamic",
    project_urls={
        "Source Code": "https://gitlab.com/lucyvin/time_template"
        },
)